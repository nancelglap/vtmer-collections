> VTMER官网前端仓库
---
### 更新说明


- 1、`git clone`到本地
- 2、每次更改后`git status`查看相应修改的文件
- 3、`git add filename` 
- 4、`git commit -m "content"` content写更改的内容注释
- 5、`git pull origin master:branch` branch为你分支的名，如果没有会自动创建
- 6、`push`之后登码云`pull request`你分支到`master`
- 7、push之前先`git pull origin master`